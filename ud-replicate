#! /bin/bash

#   Copyright (c) 1999-2001  Jason Gunthorpe <jgg@debian.org>
#   Copyright (c) 2002-2003,2006  Ryan Murray <rmurray@debian.org>
#   Copyright (c) 2004-2005  Joey Schulze <joey@infodrom.org>
#   Copyright (c) 2008,2011 Peter Palfrader <peter@palfrader.org>
#   Copyright (c) 2008 Stephen Gran <sgran@debian.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

set -e

# Without effect on the commandline
if [ -z "$TERM" -o "$TERM" = "dumb" ]
then
    exec > /dev/null 2>&1
    sleep $(( $RANDOM % 120 ))
else
    verbose=-v
fi

tempdir=''
tempfile=''
tempfile2=''

cleanup ()
{
  rm -rf $tempdir
  rm -f  $tempfile
  rm -f  $tempfile2
}

PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin
export PATH
HOST=`hostname -f`
SYNCHOST=`ud-config synchost`
LOCALSYNCON=`ud-config localsyncon`
EMAILAPPEND=`ud-config emailappend`
cd /var/lib/misc
exec 200< "."
if ! flock -w 60 -e 200; then
    log "Cannot acquire lock on `pwd`"
    exit 1
fi

trap cleanup exit

case $HOST in
$LOCALSYNCON)
    udhost=
    ;;
*)
    udhost="sshdist@$SYNCHOST:"
    ;;
esac

tempfile=$(mktemp)
tempfile2=$(mktemp)

if [ -e /var/lib/misc/thishost/dns-sshfp ]; then
  cp /var/lib/misc/thishost/dns-sshfp $tempfile
fi

if [ -e /var/lib/misc/thishost/dns-zone ]; then
  cp /var/lib/misc/thishost/dns-zone $tempfile2
fi

rsync ${verbose} --delete-after --times -e 'ssh -i /etc/ssh/ssh_host_rsa_key -o ControlPath=/var/run/.ud-replicate.ssh.socket -o ControlMaster=auto -o ControlPersist=2h -o BatchMode=yes' -rp "${udhost}/var/cache/userdir-ldap/hosts/$HOST" .

rm -f __db.passwd.db.t
makedb "$HOST/passwd.tdb" -o passwd.db.t
if [ -s "$HOST/shadow.tdb" ]
then
    rm -f __db.shadow.db.t
    (umask 027 && makedb "$HOST/shadow.tdb" -o shadow.db.t)
    chown root:shadow shadow.db.t
    chmod 0640 shadow.db.t
    mv -f shadow.db.t shadow.db
fi
rm -f __db.group.db.t
makedb "$HOST/group.tdb" -o group.db.t
mv -f passwd.db.t passwd.db
mv -f group.db.t group.db
if [ -e "$HOST/ssh-rsa-shadow" ]; then
	ln -sf $HOST/ssh-rsa-shadow .
	ln -sf `pwd -P`/ssh-rsa-shadow /etc/ssh
else
	rm -f ssh-rsa-shadow /etc/ssh/ssh-rsa-shadow
fi
ln -sf $HOST/ssh_known_hosts .
ln -sf `pwd -P`/ssh_known_hosts /etc/ssh

if [ -e ${HOST}/ssh-keys.tar.gz ]; then
  export TMPDIR='/tmp/'
  tempdir=$(mktemp -d)
  tar -C "$tempdir" -xf ${HOST}/ssh-keys.tar.gz
  mkdir -p userkeys
  chmod 755 $tempdir
  rsync -a --delete-after $tempdir/ userkeys/
fi

if [ -e ${HOST}/web-passwords ]; then
	chown root:www-data ${HOST}/web-passwords
	chmod 0640 ${HOST}/web-passwords
fi

if [ -d "/etc/exim4" ] && [ -e "${HOST}/mail-passwords" ]; then
    chown root:Debian-exim "${HOST}/mail-passwords"
    chmod 0440 "${HOST}/mail-passwords"
fi

if [ -d "/etc/exim4" -a -e "$HOST/bsmtp" ]; then
	if perl -e 'exit !((stat "/etc/exim4/bsmtp")[9] < time()-3600)'; then
		cp "$HOST/bsmtp" /etc/exim4/bsmtp
	fi
fi
if [ -d "/etc/postfix" -a -f "$HOST/forward-alias" ]; then
	sed -e "s/:/@$EMAILAPPEND/" $HOST/forward-alias > /etc/postfix/debian
	/usr/sbin/postmap hash:/etc/postfix/debian < /etc/postfix/debian || true
fi

rebuild_zones=0
if [ -e /var/lib/misc/thishost/dns-sshfp ]; then
  if ! cmp -s /var/lib/misc/thishost/dns-sshfp "$tempfile"; then
    rebuild_zones=1
  fi
fi

if [ -e /var/lib/misc/thishost/dns-zone ]; then
  if ! cmp -s /var/lib/misc/thishost/dns-zone "$tempfile2"; then
    rebuild_zones=1
  fi
fi

if [ "${rebuild_zones}" -gt 0 ]; then
  sudo -u dnsadm /srv/dns.torproject.org/bin/update
fi

if [ -d "/etc/freeradius" -a -e "$HOST/rtc-passwords" ]; then
	if ! cmp -s "$HOST/rtc-passwords" /var/local/rtc-passwords.freerad; then
		install -o freerad -g freerad -m 400 "$HOST/rtc-passwords" /var/local/rtc-passwords.freerad
		service freeradius reload
	fi
fi

if [ -d "/etc/reTurn" -a -e "$HOST/rtc-passwords" ]; then
	if ! cmp -s "$HOST/rtc-passwords" /var/local/rtc-passwords.return; then
		install -o return -g return -m 400 "$HOST/rtc-passwords" /var/local/rtc-passwords.return
		service resiprocate-turn-server reload
	fi
fi

if [ -d "/etc/prosody" -a -e "$HOST/rtc-passwords" ]; then
	if ! cmp -s "$HOST/rtc-passwords" /var/local/rtc-passwords.prosody; then
		install -o prosody -g prosody -m 400 "$HOST/rtc-passwords" /var/local/rtc-passwords.prosody
		service prosody reload
	fi
fi

if [ -d "/etc/dovecot" -a -e "$HOST/mail-passwords" ]; then
	dove_priv="/etc/dovecot/private"
	[ ! -d "${dove_priv}" ] && install -d -o root -g dovecot -m 0550 "${dove_priv}"
	if ! cmp -s "$HOST/mail-passwords" /etc/dovecot/private/mail-passwords; then
		# By default, /etc/dovecot/private is owned by root:root
		chgrp dovecot "${dove_priv}"
		install -o root -g dovecot -m 440 "$HOST/mail-passwords" "${dove_priv}/mail-passwords"
		service dovecot reload
	fi
fi
